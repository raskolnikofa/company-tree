<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index')->name('main');

Auth::routes();

Route::group(['middleware' => ['auth']], function () {

    Route::get('/home', 'WorkerController@index')->name('home');

    Route::get('/create', 'WorkerController@create')->name('create');
    Route::post('/store', 'WorkerController@store')->name('store');

    // load chiefs for hierarchy level (ajax)
    Route::get('/chiefsBy/{level}', 'WorkerPositionController@show');

    // sort by field using type (ajax)
    Route::get('/sortBy/{field}/{type}', 'SortController@index');

    // search (ajax)
    Route::get('/search/{value}', 'SearchController@index');

    Route::get('/edit/{worker}', 'WorkerController@edit')->name('edit');
    Route::put('/update/{worker}', 'WorkerController@update')->name('update');

    Route::delete('/delete/{worker}', 'WorkerController@destroy')->name('delete');
});

// show more workers for main page (ajax)
Route::get('/{id}', 'MainController@show');