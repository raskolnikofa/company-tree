<?php

use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
    Worker Factory
|--------------------------------------------------------------------------
*/

    /** @var \Illuminate\Database\Eloquent\Factory $factory */
    $factory->defineAs(App\Worker::class, 'CEO', function (Faker\Generator $faker) {
        return [
            'name' => $faker->unique()->name,
            'salary' => '30000',
            'position' => 'CEO',
            'date' => $faker->dateTimeBetween('-10 years', 'now'),
            'chief_id' => null,
            'level' => 1,
            'created_at' => Carbon::now()
        ];
    });

    /** @var \Illuminate\Database\Eloquent\Factory $factory */
    $factory->defineAs(App\Worker::class, 'director', function (Faker\Generator $faker) {
        return [
            'name' => $faker->unique()->name,
            'salary' => $faker->numberBetween(20000,30000),
            'position' => 'director',
            'date' => $faker->dateTimeBetween('-10 years', 'now'),
            'chief_id' => \App\Worker::where(['position' => 'CEO'])
                ->get()
                ->random()->id,
            'level' => 2,
            'created_at' => Carbon::now()
        ];
    });

    /** @var \Illuminate\Database\Eloquent\Factory $factory */
    $factory->defineAs(App\Worker::class, 'chief', function (Faker\Generator $faker) {
        return [
            'name' => $faker->unique()->name,
            'salary' => $faker->numberBetween(15000,20000),
            'position' => 'chief',
            'date' => $faker->dateTimeBetween('-10 years', 'now'),
            'chief_id' => \App\Worker::where(['position' => 'director'])
                ->get()
                ->random()->id,
            'level' => 3,
            'created_at' => Carbon::now()
        ];
    });

    /** @var \Illuminate\Database\Eloquent\Factory $factory */
    $factory->defineAs(App\Worker::class, 'supervisor', function (Faker\Generator $faker) {
        return [
            'name' => $faker->unique()->name,
            'salary' => $faker->numberBetween(10000,15000),
            'position' => 'supervisor',
            'date' => $faker->dateTimeBetween('-7 years', 'now'),
            'chief_id' => \App\Worker::where(['position' => 'chief'])
                ->get()
                ->random()->id,
            'level' => 4,
            'created_at' => Carbon::now()
        ];
    });

    /** @var \Illuminate\Database\Eloquent\Factory $factory */
    $factory->defineAs(App\Worker::class, 'operator', function (Faker\Generator $faker) {
        return [
            'name' => $faker->unique()->name,
            'salary' => $faker->numberBetween(7000,10000),
            'position' => 'operator',
            'date' => $faker->dateTimeBetween('-5 years', 'now'),
            'chief_id' => \App\Worker::where(['position' => 'supervisor'])
                ->get()
                ->random()->id,
            'level' => 5,
            'created_at' => Carbon::now()
        ];
    });

    /** @var \Illuminate\Database\Eloquent\Factory $factory */
    $factory->defineAs(App\Worker::class, 'worker', function (Faker\Generator $faker) {
        return [
            'name' => $faker->unique()->name,
            'salary' => $faker->numberBetween(5000,10000),
            'position' => 'worker',
            'date' => $faker->dateTimeBetween('-10 years', 'now'),
            'chief_id' => \App\Worker::where(['position' => 'operator'])
                ->get()
                ->random()->id,
            'level' => 6,
            'created_at' => Carbon::now()
        ];
    });
