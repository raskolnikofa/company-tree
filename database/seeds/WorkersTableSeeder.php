<?php

use Illuminate\Database\Seeder;

class WorkersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create main CEO
        factory(\App\Worker::class, 'CEO', 1)->create();

        // create 2 directors
        factory(\App\Worker::class, 'director', 2)->create();

        // create 50 chiefs
        factory(\App\Worker::class, 'chief', 50)->create();

        // create 600 supervisors
        factory(\App\Worker::class, 'supervisor', 3000)->create();

        // create 18 000 operators
        factory(\App\Worker::class, 'operator', 6000)->create();

        // create 90 000 workers
        factory(\App\Worker::class, 'worker', 8000)->create();
    }
}
