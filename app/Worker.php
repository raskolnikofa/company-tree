<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;

class Worker extends Model
{
    protected $fillable = [
        'name',
        'position',
        'salary',
        'chief_id',
        'level',
        'img_path'
    ];

    // Get all available worker positions for dropdown
    public function scopeGetAllPositions($query)
    {
        return $query->select('position', 'level')->distinct()->get();
    }

    // Get chiefs for current worker position
    public function scopeGetChiefsByWorkerPositionLevel($query, $level)
    {
        return $query->where(['level' => $level])->get();
    }

    // Explode position field for 'level'
    public function getLevelByPositionField($position)
    {
        return $explode = explode('|', $position);
    }

    // search
    public function scopeGetWorkersBySearch($query, $value)
    {
        return $query->where('name', 'like', "%" . $value . "%")
            ->orWhere('position', 'like', "%" . $value . "%")
            ->orWhere('salary', 'like', "%" . $value . "%")
            ->orWhere('date', 'like', "%" . $value . "%")
            ->orWhere('chief_id', 'like', "%" . $value . "%")
            ->paginate(20);
    }

    // sorting
    public function scopeGetSortableWorkers($query, $field, $type)
    {
        return $query->orderBy($field, $type)->paginate(20);
    }

    // Store new worker (include image)
    public function storeNewWorker($request)
    {
        if ($request->file('img')) {
            $this->img_path = $this->storeImage($request);
        }

        // position field has 2 value, we need to explode it for 'position' and 'level'
        $position_level = $this->getLevelByPositionField($request->position);

        $this->name = $request->name;
        $this->salary = $request->salary;
        $this->date = $request->date;
        $this->position = $position_level[0];
        $this->level = $position_level[1];
        $this->chief_id = $request->chief_id;

        $this->save();
    }

    // Update worker (include image)
    public function updateStoredWorker($worker, $request)
    {
        if ($request->file('img')) {
            $worker->img_path = $this->updateImage($worker->img_path, $request);
        }

        // position field has 2 value, we need to explode it for 'position' and 'level'
        $position_level = $this->getLevelByPositionField($request->position);

        $worker->name = $request->name;
        $worker->salary = $request->salary;
        $worker->date = $request->date;

        // if worker change position, redistribute other workers
        if ($worker->position != $position_level[0]) {
            $this->redistributeWorkers($worker);

            $worker->position = $position_level[0];
            $worker->level = $position_level[1];
        }

        $worker->chief_id = $request->chief_id;

        $worker->update();
    }

    // Store image for new worker.
    public function storeImage($request)
    {
        $img = $request->file('img');
        $extension = $img->getClientOriginalExtension();
        $path_title = uniqid('worker_', false) . '.' . $extension;

        if ($img) {
            $img->move(storage_path() . '/app/public/img/', $path_title);
        }

        $thumbnailImage = Image::make(storage_path() . '/app/public/img/'.$path_title);
        $thumbnailImage->resize(50, 50);
        $thumbnailImage->save(storage_path() . '/app/public/img/thumbs/'.$path_title);

        return $path_title;
    }

    // Update image for worker.
    public function updateImage($img_path, $request)
    {
        $img = $request->file('img');
        $extension = $img->getClientOriginalExtension();
        $path_title = uniqid('worker', false) . '.' . $extension;

        Storage::disk('public')->delete('/img/'.$img_path);
        Storage::disk('public')->delete('/img/thumbs/'.$img_path);

        $img->move(storage_path() . '/app/public/img/', $path_title);

        $thumbnailImage = Image::make(storage_path() . '/app/public/img/'.$path_title);
        $thumbnailImage->resize(50, 50);
        $thumbnailImage->save(storage_path() . '/app/public/img/thumbs/'.$path_title);

        return $path_title;
    }

    // Destroy worker
    public function destroyWorker($worker)
    {
        Storage::disk('public')->delete('/img/'.$worker->img_path);
        Storage::disk('public')->delete('/img/thumbs/'.$worker->img_path);
        $this->redistributeWorkers($worker);
        $worker->delete();
    }

    // Redistribute workers if delete chief or change chief position
    public function redistributeWorkers($chief)
    {
        $workers = DB::table('workers')->where(['chief_id' => $chief->id])->get();

        foreach ($workers as $worker) {
            $random_chief_id = DB::table('workers')->where([['position' ,'=', $chief->position], ['id', '!=', $chief->id]])->get()->random()->id;
            DB::table('workers')->where('id', '=', $worker->id)->update(['chief_id' => $random_chief_id]);
        }
    }
}
