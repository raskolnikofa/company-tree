<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../../vendor/twbs/bootstrap/dist' => public_path('bootstrap/'),
        ], 'public');

        $this->publishes([
            __DIR__ . '/../../vendor/components/font-awesome/css' => public_path('font-awesome/css'),
            __DIR__ . '/../../vendor/components/font-awesome/webfonts' => public_path('font-awesome/webfonts')
        ], 'public');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
