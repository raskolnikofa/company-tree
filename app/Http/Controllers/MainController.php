<?php

namespace App\Http\Controllers;

use App\Worker;
use Illuminate\Http\Request;

class MainController extends Controller
{

    /**
     * Show the application Main page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workers = Worker::where(['chief_id' => null])->get();
        return view('pages.welcome', compact('workers'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $workers = Worker::where(['chief_id' => $id])->get();
        return response()->json($workers);
    }
}
