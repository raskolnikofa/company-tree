<?php

namespace App\Http\Controllers;

use App\Worker;

class SearchController extends Controller
{
    protected $worker;

    public function __construct(Worker $worker)
    {
        $this->worker = $worker;
    }

    /**
     * Display a listing of the resource.
     *
     * @param   $value
     * @return \Illuminate\Http\Response
     */
    public function index($value)
    {
        $workers = $this->worker->getWorkersBySearch($value);
        return response()->json($workers);
    }
}
