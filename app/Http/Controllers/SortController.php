<?php

namespace App\Http\Controllers;

use App\Worker;

class SortController extends Controller
{
    protected $worker;

    public function __construct(Worker $worker)
    {
        $this->worker = $worker;
    }

    /**
     * Display a sortable listing.
     *
     * @param   $field
     * @param   $type
     * @return \Illuminate\Http\Response
     */
    public function index($field, $type)
    {
        $workers = $this->worker->getSortableWorkers($field, $type);
        return response()->json($workers);
    }
}
