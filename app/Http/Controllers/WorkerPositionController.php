<?php

namespace App\Http\Controllers;

use App\Worker;

class WorkerPositionController extends Controller
{
    protected $worker;

    public function __construct(Worker $worker)
    {
        $this->worker = $worker;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display chiefs by hierarchy level.
     *
     * @param  \App\Worker  $level
     * @return \Illuminate\Http\Response
     */
    public function show($level)
    {
        $level = $level-1;
        $chiefs = $this->worker->getChiefsByWorkerPositionLevel($level);
        return response()->json($chiefs);
    }
}
