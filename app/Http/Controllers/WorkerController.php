<?php

namespace App\Http\Controllers;

use App\Worker;
use App\Http\Requests\StoreWorker;
use App\Http\Requests\UpdateWorker;

class WorkerController extends Controller
{
    protected $worker;

    public function __construct(Worker $worker)
    {
        $this->worker = $worker;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workers = Worker::paginate(20);

        return view('pages.home', compact('workers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $positions = $this->worker->getAllPositions();

        return view('pages.create', compact('positions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreWorker $request)
    {
        $this->worker->storeNewWorker($request);
        return redirect()->route('home')->with('status', 'Successfully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function show(Worker $worker)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function edit(Worker $worker)
    {
        $positions = $this->worker->getAllPositions();
        $level = $worker->level-1;
        $chiefs = $this->worker->getChiefsByWorkerPositionLevel($level);

        return view('pages.edit', compact('worker', 'positions', 'chiefs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  StoreWorker  $request
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateWorker $request, Worker $worker)
    {
        $this->worker->updateStoredWorker($worker, $request);
        return redirect()->route('home')->with('status', 'Successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function destroy(Worker $worker)
    {
        $this->worker->destroyWorker($worker);
        return redirect()->route('home')->with('status', 'Successfully deleted!');
    }
}
