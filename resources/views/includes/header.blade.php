<nav class="navbar navbar-expand-lg bg-info">

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Left Side Of Navbar -->
        <ul class="navbar-nav mr-auto">
            <li>
                <a class="navbar-brand text-light" href="{{ url('/') }}" rel="tooltip" title="Workers run the world" data-placement="bottom">
                    <img src="{{ asset('logo.png') }}"> <h6>Workers</h6>
                </a>
            </li>
            @auth
                <li>
                    <a class="navbar-brand text-light" href="{{ url('home') }}" data-placement="bottom">Dashboard</a>
                </li>
                <li>
                    <a class="navbar-brand text-light" href="{{ url('create') }}" data-placement="bottom">Add new worker</a>
                </li>
            @endauth
        </ul>

        <!-- Right Side Of Navbar -->
        <ul class="navbar-nav ml-auto">
            <!-- Authentication Links -->
            @if (Auth::guest())
                <li class="nav-item"><a class="nav-link text-light" href="{{ route('login') }}">Login</a></li>
                <li class="nav-item"><a class="nav-link text-light" href="{{ route('register') }}">Register</a></li>
            @else
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle text-light" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right" role="menu">
                        <li class="nav-item text-light">
                            <a class="nav-link text-info"
                               href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            @endif
        </ul>
    </div>
</nav>