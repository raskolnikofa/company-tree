@extends('layouts.app')

@section('content')
    <div class="container-fluid">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="POST" action="/store" class="needs-validation" novalidate="" enctype="multipart/form-data">

            {{ csrf_field() }}

            <div class="m-3">
               <label class="btn btn-default">
                        <span class="input-group-text" id="basic-addon1">
                            Select image
                        </span>
                   <input type="file" name="img"
                          onchange='$("#upload-file-info").html($(this).val());'
                          hidden
                   >
                   <span class='text-success' id="upload-file-info"></span>
               </label>
            </div>

            <div class="m-3">
                <label for="name">Name</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name')}}" required>
                    <div class="invalid-feedback">
                        Name is required.
                    </div>
                </div>
            </div>

            <div class="m-3">
                <label for="position">Position</label>
                <select class="form-control" id="position" name="position">
                    @forelse($positions as $position)
                        <option value="{{ $position->position }}|{{ $position->level }}"
                                data-level="{{ $position->level }}"
                                {{ old('position') ==  $position->position .'|'. $position->level  ? 'selected' : '' }}
                        >{{ $position->position }}</option>
                    @empty
                        No position is found.
                    @endforelse
                </select>
            </div>

            <div class="m-3">
                <label for="salary">Salary</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="salary" name="salary" value="{{ old('salary')}}" required>
                    <div class="invalid-feedback">
                        Salary is required.
                    </div>
                </div>
            </div>

            <div class="m-3">
                <label for="date">Date</label>
                <div class="input-group">
                    <input class="form-control" type="date" name="date" value="" id="create_date">
                </div>
            </div>

            <div class="m-3">
                <label for="chief">Chief</label>
                <select class="form-control" id="chief" name="chief_id">
                </select>
            </div>

            <input type="submit" class="btn btn-round btn-info m-3" value="Save">

        </form>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('/js/form.js') }}"></script>
@endpush