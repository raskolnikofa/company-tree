@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <ul class="list-group row">
            @forelse ( $workers as $worker)
            <li class="list-group-item"><span class="chief h1" data-id="{{ $worker->id }}">{{ $worker->name }} | <cite class="text-info">{{ $worker->position }}</cite></span></li>
            @empty
                Nothing to show.
            @endforelse
        </ul>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('/js/welcome.js') }}"></script>
@endpush
