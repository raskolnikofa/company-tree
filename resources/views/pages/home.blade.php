@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                            <div class="row m-2">
                                    <form class="form-inline my-2 col-md-12">
                                        <input class="form-control mr-sm-2" type="text" name="search" aria-label="Search">
                                        <input type="button" id="search" class="btn btn-dark m-2" value="Search">
                                    </form>
                            </div>

                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="w-10">
                                        <i class="fas fa-angle-up sortable" data-field="img_path" data-type="ASC"></i>
                                        <i class="fas fa-angle-down sortable"  data-field="img_path" data-type="DESC"></i></th>
                                    </th>
                                    <th class="w-30">Name
                                        <i class="fas fa-angle-up sortable" data-field="name" data-type="ASC"></i>
                                        <i class="fas fa-angle-down sortable"  data-field="name" data-type="DESC"></i></th>
                                    <th class="w-10">Position
                                        <i class="fas fa-angle-up sortable"  data-field="position" data-type="ASC"></i>
                                        <i class="fas fa-angle-down sortable"  data-field="position" data-type="DESC"></i></th>
                                    <th class="w-10">Salary
                                        <i class="fas fa-angle-up sortable" data-field="salary" data-type="ASC"></i>
                                        <i class="fas fa-angle-down sortable" data-field="salary" data-type="DESC"></i></th>
                                    <th class="w-10">Date
                                        <i class="fas fa-angle-up sortable" data-field="date" data-type="ASC"></i>
                                        <i class="fas fa-angle-down sortable" data-field="date" data-type="DESC"></i></th>
                                    <th class="w-10">
                                        Chief
                                        <i class="fas fa-angle-up sortable" data-field="chief_id" data-type="ASC"></i>
                                        <i class="fas fa-angle-down sortable" data-field="chief_id" data-type="DESC"></i></th>
                                    </th>
                                    <th class="w-10"></th>
                                    <th class="w-10"></th>
                                </tr>
                                </thead>
                                <tbody id="content">
                                @forelse ($workers as $worker)
                                    <tr>
                                        <td>
                                            <img
                                                    @if ( $worker->img_path != null)
                                                    src="{{ asset('storage/img/thumbs/'.$worker->img_path) }}"
                                                    @else
                                                    src="{{ asset('default.png') }}" style="height: 50px;"
                                                    @endif
                                                    class="rounded"
                                            >
                                        </td>
                                        <td>{{ $worker->name }}</td>
                                        <td>{{ $worker->position }}</td>
                                        <td>{{ $worker->salary }}</td>
                                        <td>{{ $worker->date }}</td>
                                        <td>{{ $worker->chief_id }}</td>
                                        <th><a href="{{ url('edit', ['worker' => $worker->id]) }}" class="btn btn-info btn-round">Edit</a></th>
                                        <th>
                                            <form method="POST" action="{{ url('delete', ['worker' => $worker->id]) }}">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <input type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?');" value="Delete">
                                            </form>
                                        </th>
                                    </tr>
                                @empty
                                    <tr class="table-light">
                                        <th scope="row"></th>
                                        <td></td>
                                        <td>Nobody to show.</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>

        </div>

    {{ $workers->links('vendor.pagination.bootstrap-4') }}

</div>
    <script>
        $(document).on('click','.sortable',function(e){
            e.preventDefault();

            let field = $(this).data('field');
            let type = $(this).data('type');

            $.ajax({
                url : '/sortBy/'+field+'/'+type,
                method : 'GET',
                success : function (data)
                {
                    $('#content').empty();

                    let workers = '';
                    $.each(data.data, function (key, value) {
                        workers +=
                            '<tr><td>' +

                            '<img src="/storage/img/thumbs/' + value['img_path'] + '" class="rounded" onerror="this.src=\'default.png\';" style="height: 50px;" >'

                            +
                            '</td><td>' + value['name'] +
                            '</td><td>' + value['position'] +
                            '</td><td>' + value['salary'] +
                            '</td><td>' + value['date'] +
                            '</td><td>' + value['chief_id'] +
                            '</td><td>' + '<a href="/edit/'+ value['id']+'" class="btn btn-info btn-round">Edit</a>' +
                            '</td><td>' + '<form method="POST" action="/delete/'+ value['id'] +'">' +
                            '{{ method_field("DELETE") }}' +
                            '{{ csrf_field() }}' +
                            '<input type="submit" class="btn btn-danger" onclick="return confirm(\'Are you sure?\');" value="Delete">' +
                            '</form>' +
                            '</td></tr>';
                    });

                    $('#content').append(workers);
                }
            });
        });


        $(document).on('click','#search',function(e){
            e.preventDefault();

            let search = $("input[name='search']").val();

            $.ajax({
                url : '/search/'+search,
                method : 'GET',
                success : function (data)
                {
                    $('#content').empty();

                    let workers = '';
                    $.each(data.data, function (key, value) {
                        workers +=
                            '<tr><td>' +

                            '<img src="/storage/img/thumbs/' + value['img_path'] + '" class="rounded" onerror="this.src=\'default.png\';" style="height: 50px;" >'

                            +
                            '</td><td>' + value['name'] +
                            '</td><td>' + value['position'] +
                            '</td><td>' + value['salary'] +
                            '</td><td>' + value['date'] +
                            '</td><td>' + value['chief_id'] +
                            '</td><td>' + '<a href="/edit/'+ value['id']+'" class="btn btn-info btn-round">Edit</a>' +
                            '</td><td>' + '<form method="POST" action="/delete/'+ value['id'] +'">' +
                            '{{ method_field("DELETE") }}' +
                            '{{ csrf_field() }}' +
                            '<input type="submit" class="btn btn-danger" onclick="return confirm(\'Are you sure?\');" value="Delete">' +
                            '</form>' +
                            '</td></tr>';
                    });

                    $('#content').append(workers);
                }
            });
        });
    </script>
@endsection

@push('scripts')
    <script src="{{ asset('/js/homescript.js') }}"></script>
@endpush

