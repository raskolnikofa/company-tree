@extends('layouts.app')

@section('content')
    <div class="container-fluid">

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form method="POST" action="/update/{{ $worker->id }}" class="needs-validation" novalidate="" enctype="multipart/form-data">
                    {{ method_field('PUT') }}

                    {{ csrf_field() }}


                    <div class="row">
                        <div class="m-3 thumbnail">
                            <img
                                    @if ( $worker->img_path != null)
                                    src="{{ asset('storage/img/'.$worker->img_path) }}"
                                    @else
                                    src="{{ asset('default.png') }}"
                                    @endif
                                    style="height: 200px;"
                                    class="rounded"
                            >
                        </div>


                        <div class="m-3" style="height: 200px;">
                            <label class="btn btn-default">
                                <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    Select new image
                                                                </span>
                                </div>
                                <span class='text-danger'><i>Old image will be deleted</i></span>
                                <input type="file" name="img"
                                       onchange='$("#upload-file-info").html($(this).val());'
                                       hidden
                                >
                                <span class='text-success' id="upload-file-info"></span>
                            </label>
                        </div>
                    </div>


                    <div class="m-3">
                        <label for="name">Name</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="name" name="name" value="{{ $worker->name }}" required>
                            <div class="invalid-feedback">
                                Name is required.
                            </div>
                        </div>
                    </div>

                    <div class="m-3">
                        <label for="position">Position</label>
                        <select class="form-control" id="position" name="position">
                            @forelse($positions as $position)
                                <option value="{{ $position->position }}|{{ $position->level }}"
                                        @if( $worker->position == $position->position ) selected @endif
                                        data-level="{{ $position->level }}"
                                >{{ $position->position }}</option>
                            @empty
                                No position is found.
                            @endforelse
                        </select>
                    </div>

                    <div class="m-3">
                        <label for="salary">Salary</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="salary" name="salary" value="{{ $worker->salary }}" required>
                            <div class="invalid-feedback">
                                Salary is required.
                            </div>
                        </div>
                    </div>

                    <div class="m-3">
                        <label for="date">Date</label>
                        <div class="input-group">
                            <input class="form-control" type="date" name="date" id="date" value="{{ $worker->date }}">
                        </div>
                    </div>

                    <div class="m-3">
                        <label for="chief">Chief</label>
                        <select class="form-control" id="chief" name="chief_id">
                            @forelse($chiefs as $chief)
                                <option value="{{ $chief->id }}"
                                @if( $worker->chief_id == $chief->id ) selected @endif
                                >{{ $chief->name }} - {{ $chief->position }}</option>
                            @empty
                                No chief is found for this worker.
                            @endforelse
                        </select>
                    </div>

                    <input type="submit" class="btn btn-round btn-info m-3" value="Save">

                </form>
    </div>

@endsection

@push('scripts')
    <script src="{{ asset('/js/form.js') }}"></script>
@endpush