$(document).ready(function(){
    $(document).on('change','#position',function(e){
        e.preventDefault();

        let level = $(this).children("option:selected").data('level');

        $.ajax({
            url : '/chiefsBy/'+level,
            method : 'GET',
            success : function (data)
            {
                $('#chief').empty();
                $.each( data, function( key, value ) {
                    $('#chief').append($("<option></option>")
                        .attr("value", value['id']).text(value['name']+' - '+value['position']));
                });
            }
        });
    });

    $('#create_date').val(new Date().toISOString().split('T')[0]);
});