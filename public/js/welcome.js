$(document).ready(function(){
    $(document).on('click','.chief',function(e){
        e.preventDefault();

        var id = $(this).data('id');

        // 'if' for hiding workers on second click
        if ($(this).hasClass("clicked-once")) {
            $('[data-parent-id='+id+']').remove();
            $(this).removeClass("clicked-once h3");
        } else {
            $.ajax({
                url : '/'+id,
                method : 'GET',
                success : function (data)
                {
                    let workers = '';
                    $.each( data, function( key, value ) {
                        workers += '<li class="list-group-item" data-parent-id="'+ id +'">' +
                            '<span class="chief" data-id="'+ value['id'] +'">' +
                            '' + value['name'] + ' | <cite class="text-info">' + value['position'] + '' +
                            '</cite></span></li>';
                    });

                    $('[data-id='+id+']').after(workers).addClass("clicked-once h3");
                }
            });
        }
    });
});