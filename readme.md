

##Commits

- Create Model, Migration and Controller (php artisan make:model -mcr). 
Create Seeder and Factory. All for Worker.

- Add BS4 and make 'general' view files. Also make basic routing.

- CRUD (REST). Install Intervention and link Storage to public folder. Create Requests for create and update.

- Add search and sorting. Install Font-Awesome.